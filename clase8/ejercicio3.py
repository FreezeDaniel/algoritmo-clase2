#Un algoritmo que, ingresando la edad de una persona , considere 
# lo siguiente:
# Si el usuario es menor de 18 años, le saldrá, debajo de su 
# nombre completo y año de nacimiento, el mensaje: “Eres menor de 
# edad, no podemos darte de alta hasta el año XXXX”)
# Si es mayor de 18 años y menor de 25, le saldrá el mensaje: 
# “Tienes un 10% de descuento”
# Si es mayor de 25 años, le saldrá el mensaje: “Lo sentimos, 
# pero no tienes descuento”
# Si tiene justo 18 o 25 años, le saldrá el mensaje: “Premio, 
# tienes un descuento especial del 20%”

# Pedimos al usuario que ingrese su nombre completo y año de nacimiento
nombre_completo = input("Por favor ingresa tu nombre completo: ")
ano_nacimiento = int(input("Por favor ingresa tu año de nacimiento: "))

# Calculamos la edad del usuario
edad = 2023 - ano_nacimiento

# Evaluamos las condiciones para determinar el mensaje que se debe mostrar
if edad < 18:
    print(nombre_completo, "nacido/a en", ano_nacimiento, "- Eres menor de edad, no podemos darte de alta hasta el año", ano_nacimiento + 18)
else:
    if edad == 18 or edad == 25:
        print(nombre_completo, "nacido/a en", ano_nacimiento, "- Premio, tienes un descuento especial del 20%")
    else:
        if edad > 18 and edad < 25:
            print(nombre_completo, "nacido/a en", ano_nacimiento, "- Tienes un 10% de descuento")
        else:
            print(nombre_completo, "nacido/a en", ano_nacimiento, "- Lo sentimos, pero no tienes descuento")
